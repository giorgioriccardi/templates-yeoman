'use strict';
var generators = require('yeoman-generator');
var yosay = require('yosay');
var chalk = require('chalk');
var wiredep = require('wiredep');
var mkdirp = require('mkdirp');
var path = require('path');
var fs = require('fs');
var _s = require('underscore.string');
var execSync = require('child_process').execSync;
// const remote_linters = 'git@gitlab.com:front-end-projects/templates-linters.git';
const remote_linters = 'git@bitbucket.org:giorgioriccardi/templates-linters.git';
// const remote_linters = 'https://giorgioriccardi@bitbucket.org/giorgioriccardi/templates-linters.git';
// const remote_tests = 'git@gitlab.com:front-end-projects/templates-autotesting.git';
const remote_tests = 'git@bitbucket.org:giorgioriccardi/templates-autotesting.git';
// const remote_tests = 'https://giorgioriccardi@bitbucket.org/giorgioriccardi/templates-autotesting.git';

module.exports = generators.Base.extend({
  constructor: function () {
    var testLocal;

    generators.Base.apply(this, arguments);

    this.option('skip-welcome-message', {
      desc: 'Skips the welcome message',
      type: Boolean,
      defaults: true
    });

    this.option('skip-install-message', {
      desc: 'Skips the message after the installation of dependencies',
      type: Boolean
    });

    this.option('babel', {
      desc: 'Use Babel',
      type: Boolean,
      defaults: true
    });

    // this.option('test-framework', {
    //   desc: 'Test framework to be invoked',
    //   type: String,
    //   defaults: 'jasmine'
    // });

    // if (this.options['test-framework'] === 'mocha') {
    //   testLocal = require.resolve('generator-mocha/generators/app/index.js');
    // } else if (this.options['test-framework'] === 'jasmine') {
    //   testLocal = require.resolve('generator-jasmine/generators/app/index.js');
    // }
    //
    // this.composeWith(this.options['test-framework'] + ':app', {
    //   options: {
    //     'skip-install': this.options['skip-install']
    //   }
    // }, {
    //   local: testLocal
    // });
  },

  initializing: function () {
    this.pkg = require('../package.json');
  },

  prompting: function () {
    if (!this.options['skip-welcome-message']) {
      this.log(yosay('\'Allo \'allo! Out of the box I include HTML5 Boilerplate, jQuery, and a gulpfile to build your app.'));
    }

    var prompts = [{
      type: 'checkbox',
      name: 'features',
      message: 'Which additional features would you like to include?',
      choices: [{
        name: 'Sass',
        value: 'includeSass',
        checked: true
      }, {
        name: 'Modernizr',
        value: 'includeModernizr',
        checked: true
      }]
    }, {
      type: 'confirm',
      name: 'includeJQuery',
      message: 'Would you like to include jQuery?',
      default: true
    }];

    return this.prompt(prompts).then(function (answers) {
      var features = answers.features;

      function hasFeature(feat) {
        return features && features.indexOf(feat) !== -1;
      }

      // manually deal with the response, get back and store the results.
      // we change a bit this way of doing to automatically do this in the self.prompt() method.
      this.includeSass = hasFeature('includeSass');
      this.includeModernizr = hasFeature('includeModernizr');
      this.includeJQuery = answers.includeJQuery;

    }.bind(this));
  },

  writing: {
    gulpfile: function () {
      this.fs.copyTpl(
        this.templatePath('config/gulpfile.js'),
        this.destinationPath('gulpfile.js'),
        {
          date: (new Date).toISOString().split('T')[0],
          name: this.pkg.name,
          version: this.pkg.version,
          includeSass: this.includeSass,
          includeBabel: this.options['babel'],
          testFramework: this.options['test-framework']
        }
      );

      this.fs.copyTpl(
        this.templatePath('config/gulpfile-build.js'),
        this.destinationPath('gulpfile-build.js'),
        {
          date: (new Date).toISOString().split('T')[0],
          name: this.pkg.name,
          version: this.pkg.version,
          includeSass: this.includeSass,
          includeBabel: this.options['babel'],
          testFramework: this.options['test-framework']
        }
      );
    },

    packageJSON: function () {
      this.fs.copyTpl(
        this.templatePath('config/package.json'),
        this.destinationPath('package.json'),
        {
          includeSass: this.includeSass,
          includeBabel: this.options['babel'],
          includeJQuery: this.includeJQuery,
        }
      );

      // Extend with test devDependencies
      // execSync(`(mkdir -p test; cd test; git archive --remote=${remote_tests} HEAD package.json | tar -x)`);
      // let testPkg = require(this.destinationPath('test/package.json'));
      // this.fs.extendJSON('package.json', testPkg);
    },

    linters: function(){
      // Transfer files from linter template repo
      let command = `git archive --remote=${remote_linters} HEAD scripts | tar -x`;
      execSync(command, (error, stdout, stderr) => {
        if (error) {
          console.error(`exec error: ${error}`);
          return;
        }
      });
    },

    // tests: function(){
    //   // Transfer files from test template repo
    //   execSync(`git archive --remote=${remote_tests} HEAD test .env | tar -x`);
    //   execSync(`(mkdir -p test; cd test; git archive --remote=${remote_tests} HEAD gulpfile.js | tar -x)`);
    //   execSync('mv test/gulpfile.js gulpfile-test.js');
    // },

    // csscomb: function(){
    //   this.fs.copy(
    //     this.templatePath('config/csscomb.json'),
    //     this.destinationPath('csscomb.json')
    //   );
    // },

    git: function () {
      this.fs.copy(
        this.templatePath('config/gitignore'),
        this.destinationPath('.gitignore'));

      this.fs.copy(
        this.templatePath('config/gitattributes'),
        this.destinationPath('.gitattributes'));
    },

    bower: function () {
      this.fs.copy(
        this.templatePath('config/bower.json'),
        this.destinationPath('bower.json')
      );

      this.fs.copy(
        this.templatePath('config/bowerrc'),
        this.destinationPath('.bowerrc')
      );
    },

    h5bp: function () {
      this.fs.copy(
        this.templatePath('config/robots.txt'),
        this.destinationPath('app/robots.txt'));
    },

    styles: function () {
      this.fs.copyTpl(
        this.templatePath('www/styles'),
        this.destinationPath('app/styles')
      );
    },

    scripts: function () {
      this.fs.copy(
        this.templatePath('www/scripts'),
        this.destinationPath('app/scripts')
      );
    },

    readme: function () {
      this.fs.copy(
        this.templatePath('www/README.md'),
        this.destinationPath('README.md')
      );
    },

    html: function () {

      this.fs.copyTpl(
        this.templatePath('www/index.html'),
        this.destinationPath('app/index.html'),
        {
          appname: this.appname,
          includeSass: this.includeSass,
          includeModernizr: this.includeModernizr,
          includeJQuery: this.includeJQuery,
        }
      );
    },

    assets: function () {
      // Create asset folder and symlinks
      let dirs = ['fonts', 'images'];
      let srcPath = 'assets';
      let appPath = 'app';
      let distPath = 'dist';
      let relPath;
      let dirPath;

      mkdirp.sync(appPath);
      mkdirp.sync(distPath);

      dirs.forEach((dir) => {
        dirPath = `${srcPath}/${dir}`;
        mkdirp.sync(dirPath);

        relPath = path.relative(appPath, dirPath);
        fs.symlinkSync(relPath, path.resolve(appPath, dir), 'dir');

        relPath = path.relative(distPath, dirPath);
        fs.symlinkSync(relPath, path.resolve(distPath, dir), 'dir');
      });
    }
  },

  install: function () {
    this.installDependencies({
      skipMessage: this.options['skip-install-message'],
      skipInstall: this.options['skip-install']
    });
  },

  end: {
    function () {
      var bowerJson = this.fs.readJSON(this.destinationPath('bower.json'));
      var howToInstall =
        '\nAfter running ' +
        chalk.yellow.bold('npm install & bower install') +
        ', inject your' +
        '\nfront end dependencies by running ' +
        chalk.yellow.bold('gulp wiredep') +
        '.';

      if (this.options['skip-install']) {
        this.log(howToInstall);
        return;
      }

      // wire Bower packages to .html
      wiredep({
        bowerJson: bowerJson,
        directory: 'bower_components',
        ignorePath: /^(\.\.\/)*\.\./,
        src: 'app/index.html'
      });

      if (this.includeSass) {
        // wire Bower packages to .scss
        wiredep({
          bowerJson: bowerJson,
          directory: 'bower_components',
          ignorePath: /^(\.\.\/)+/,
          src: 'app/styles/*.scss'
        });
      }
    }
  }

});
