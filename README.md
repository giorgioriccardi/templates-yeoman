## Front End Yeoman generator

### Description:
* This is the yeoman generator for our default front end projects.  If anything needs to be updated,
it should be done here.


### Specs:
* Browser support:
  * Latest version of Chrome, Firefox, Opera, and Safari
  * IE 10 and up
  * iOS Safari (latest version)
  * Latest versions of Android Chrome, Firefox, and Opera

### How to use this template:
1. Install yeoman
`$ npm install -g yo bower grunt-cli gulp`. You may need to run this as a `sudo` command.

2. Clone this project into your workspace.  If the project already exists, do a `git pull` to get the latest version.

3. Check that you have access to Gitlab projects [templates-autotesting](https://gitlab.com/front-end-projects/templates-autotesting) and [templates-linters](https://gitlab.com/front-end-projects/templates-linters) as Yeoman will be pulling from those repositories.

4. Run `npm install`.

5. Link to your npm.  This makes the generator publically available.
```
$ cd path/to/templates-yeoman
$ npm link
```

6. In terminal, navigate to your new project folder and run `yo frontend2017` to generate the template.

### Environment and Tools:
Source code and resources are version controlled using `git`. We use `gitlab` (`gitlab.com`) as a git repository hosting and code review/management service.

We'll be using `gulp` to configure and build the project.  Before starting, run the following:

```
% npm install
% bower install
% scripts/update-env.py
```
Note: project needs to be in gitlab in order for `scripts/update-env.py` to work.

Do not commit the `bower_components` and `node_modules` folders.

#### For development
Run:
```
% gulp serve
```
This will generate a local environment that refreshes the browsers when changes are made.  Note that it will compile your scss and javascript into a `.tmp` folder which you should not commit.  Use `CTRL + c` to stop the server.


#### For staging/production
The code must be compiled before it goes to staging/production.  There are two ways you can create production ready code:

This will create production ready code and generate a local environment so you can see it:
```
% gulp serve:dist
```

Use `CTRL + c` to stop the server.

If you just want to compile the code, you can just run this:
```
% gulp build
```
In both cases, code will be compiled into the **dist** folder and this is what wil be on staging.

#### Coding standards
All developers must follow the coding standards outlined [here](https://sites.google.com/s/0B8DVJJiqXFewanpBSDFHSGN3Q2s/p/0B8DVJJiqXFewWTJBenMwcDZWWkk/edit)

To enforce these standards, developers **must** have the following installed in `Atom`:
* [editorconfig](https://atom.io/packages/editorconfig)
* [sass lint](https://atom.io/packages/linter-sass-lint)
* [eslint](https://atom.io/packages/linter-eslint)
* [docblockr](https://atom.io/packages/docblockr)

Packages that are helpful, but not required:
* [pigments](https://atom.io/packages/pigments)

### Workflow
As mentioned above, code is versioned using git. We use a single-branch continuous integration model, with integration done on the `master` branch. In more detail, a given feature or fix will be done on a feature branch (e.g. `feature/TOURISM-123-add-top-menu`) branched from master. When the branch is ready to be reviewed, a merge request is made using gitlab. Comments and changes are made within the merge request, which is finally accepted to merge the code into master. In rare cases, a merge request may be rejected, if the feature is no longer required, or a better solution is found.

### Troubleshooting
If you're getting module not found errors when trying to create a new yeoman project, it might not be installed in the node_modules folder.  Delete the `package-lock.json` file and run `npm install` again.  See [here](https://github.com/npm/npm/issues/16866) for reference.
